class Job < ActiveRecord::Base
  attr_accessible :access_key, :category_id, :description, :email, :is_active, :is_featured, :location, :logo, :name, :title, :url
  validates :access_key, :email, :category_id, :description, :is_featured, :location, :name, :title, :url, presence: true
  #mount_uploader :logo, PictureUploader
  #validates :access_key, presence: true, uniqueness: true
  #validates :email, confirmation: true
  #validates :email_confirmation, presence: true
end
