class JobsController < ApplicationController
  def index
  	@jobs = Job.all
  end

  def show
    @job = Job.find(params[:id])
  end

  def new
    @job = Job.new 
  end

  def create
    @job = Job.new(params[:job])
    if @job.save
      redirect_to jobs_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
