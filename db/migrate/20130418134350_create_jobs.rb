class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.integer :category_id
      t.string :location
      t.text :description
      t.string :name
      t.string :logo
      t.string :url
      t.string :email
      t.string :access_key
      t.boolean :is_featured
      t.boolean :is_active

      t.timestamps
    end
  end
end
